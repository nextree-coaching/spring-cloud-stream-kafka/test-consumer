package io.naradrama.consumer.event.consumer;

import io.naradrama.producer.aggregate.producer.entity.Producer;
import io.naradrama.producer.aggregate.producer.event.TestEvent;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.handler.annotation.Payload;

@EnableBinding(TestSink.class)
public class TestEventConsumer {
    //
    @StreamListener(value = TestSink.TEST_SINK, condition = "headers['eventType']=='TestEvent'")
    public void listenTestEvent(@Payload TestEvent event) {
        //
        Producer producer = event.getProducer();
        System.out.println(producer);
//        throw new RuntimeException("test");
    }
}
