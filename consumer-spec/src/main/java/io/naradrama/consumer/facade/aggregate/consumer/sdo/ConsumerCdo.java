package io.naradrama.consumer.facade.aggregate.consumer.sdo;

import lombok.Getter;
import lombok.Setter;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import io.naradrama.shared.JsonSerializable;
import io.naradrama.shared.JsonUtil;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ConsumerCdo implements JsonSerializable {
    private String id;
    private String name;

    public String toString() {
        return toJson();
    }

    public static ConsumerCdo fromJson(String json) {
        return JsonUtil.fromJson(json, ConsumerCdo.class);
    }
}
