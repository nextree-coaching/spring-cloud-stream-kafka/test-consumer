package io.naradrama.consumer.aggregate.consumer.store;

import io.naradrama.consumer.aggregate.consumer.entity.Consumer;
import java.util.List;
import io.naradrama.shared.Offset;

public interface ConsumerStore {
    void create(Consumer consumer);
    Consumer retrieve(String id);
    List<Consumer> retrieveAll(Offset offset);
    void update(Consumer consumer);
    void delete(Consumer consumer);
    boolean exists(String id);
    long countAll();
}
