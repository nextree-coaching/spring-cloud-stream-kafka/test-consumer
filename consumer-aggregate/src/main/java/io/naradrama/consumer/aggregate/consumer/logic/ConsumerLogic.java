package io.naradrama.consumer.aggregate.consumer.logic;

import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import io.naradrama.consumer.aggregate.consumer.service.ConsumerTaskService;
import io.naradrama.consumer.aggregate.consumer.store.ConsumerStore;
import org.springframework.beans.factory.annotation.Autowired;
import io.naradrama.consumer.facade.aggregate.consumer.sdo.ConsumerCdo;
import io.naradrama.consumer.aggregate.consumer.entity.Consumer;
import java.util.NoSuchElementException;
import java.util.List;
import io.naradrama.shared.Offset;
import io.naradrama.shared.NameValueList;

@Service
@Transactional
public class ConsumerLogic implements ConsumerTaskService {
    @Autowired
    private ConsumerStore consumerStore;

    @Override
    public String registerConsumer(ConsumerCdo consumerCdo) {
        Consumer consumer = new Consumer();
        consumerStore.create(consumer);
        return consumer.getId();
    }

    @Override
    public Consumer findConsumer(String consumerId) {
        Consumer consumer = consumerStore.retrieve(consumerId);
        if (consumer == null) {
            throw new NoSuchElementException("Consumer id: " + consumerId);
        }
        return consumer;
    }

    @Override
    public List<Consumer> findAllConsumers() {
        List<Consumer> consumers = consumerStore.retrieveAll(Offset.newOne(0, 100));
        if (consumers.isEmpty()) {
            throw new NoSuchElementException("Consumer");
        }
        return consumers;
    }

    @Override
    public void modifyConsumer(String consumerId, NameValueList nameValues) {
        Consumer consumer = consumerStore.retrieve(consumerId);
        consumer.setValues(nameValues);
        consumerStore.update(consumer);
    }

    @Override
    public void removeConsumer(String consumerId) {
        Consumer consumer = consumerStore.retrieve(consumerId);
        consumerStore.delete(consumer);
    }

    @Override
    public long countAllConsumer() {
        return consumerStore.countAll();
    }
}
