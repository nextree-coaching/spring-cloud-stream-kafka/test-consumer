package io.naradrama.consumer.aggregate.consumer.store.jpastore.jpo;

import io.naradrama.shared.JsonSerializable;
import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;
import io.naradrama.consumer.aggregate.consumer.entity.Consumer;
import org.springframework.beans.BeanUtils;

import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "CONSUMER")
public class ConsumerJpo implements JsonSerializable {
    @Id
    private String id;
    private String name;
    private int sequence;
    private String email;

    public ConsumerJpo(Consumer consumer) {
        BeanUtils.copyProperties(consumer, this);
    }

    public Consumer toDomain() {
        Consumer consumer = new Consumer(getId());
        BeanUtils.copyProperties(this, consumer);
        return consumer;
    }

    public static List<Consumer> toDomains(List<ConsumerJpo> consumerJpos) {
        return consumerJpos.stream().map(ConsumerJpo::toDomain).collect(Collectors.toList());
    }

    public String toString() {
        return toJson();
    }

    public static ConsumerJpo sample() {
        return new ConsumerJpo(Consumer.sample());
    }

    public static void main(String[] args) {
        System.out.println(sample());
    }
}
