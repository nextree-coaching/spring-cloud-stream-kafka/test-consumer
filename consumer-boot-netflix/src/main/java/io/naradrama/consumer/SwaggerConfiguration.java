package io.naradrama.consumer;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.paths.RelativePathProvider;
import springfox.documentation.spring.web.plugins.Docket;

import javax.servlet.ServletContext;

@Configuration
@Profile({"gcp", "kafka-clustering"})
public class SwaggerConfiguration {
    //
    @Value("${spring.application.name}")
    private String applicationName;

    @Bean
    public Docket api(ServletContext servletContext) {
        //
        return new Docket(DocumentationType.SWAGGER_2).pathProvider(new RelativePathProvider(servletContext) {
                @Override
                public String getApplicationBasePath() {
                    //
                    return "/api/" + applicationName;
                }
        });
    }
}
