package io.naradrama.consumer.aggregate.consumer.store.jpastore.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import io.naradrama.consumer.aggregate.consumer.store.jpastore.jpo.ConsumerJpo;

public interface ConsumerRepository extends PagingAndSortingRepository<ConsumerJpo, String> {
}
