package io.naradrama.consumer.event.consumer;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;

public interface TestSink {
    //
    String TEST_SINK = "test-input";
    
    @Input(TEST_SINK)
    SubscribableChannel input();
}
