package io.naradrama.consumer.rest.aggregate.consumer;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import io.naradrama.consumer.aggregate.consumer.service.ConsumerTaskFacade;
import io.naradrama.consumer.aggregate.consumer.entity.Consumer;
import org.springframework.web.bind.annotation.GetMapping;
import java.util.List;
import io.naradrama.shared.NameValueList;

@RestController
@RequestMapping("/consumer/consumers")
public class ConsumerResource implements ConsumerTaskFacade {

    @Override
    @GetMapping("{consumerId}")
    public Consumer findConsumer(String consumerId) {
        return null;
    }

    @Override
    @GetMapping
    public List<Consumer> findAllConsumers() {
        return null;
    }

    @Override
    public void modifyConsumer(String consumerId, NameValueList nameValues) {
    }

    @Override
    public void removeConsumer(String consumerId) {
    }

    @Override
    public long countAllConsumer() {
        return 0l;
    }

    @GetMapping("test")
    public String test(){
        //
        System.out.println("consumer test called");
        return "Hello from consumer";
    }
}
