package io.naradrama.consumer.client.aggregate.consumer;

import org.springframework.stereotype.Component;
import io.naradrama.consumer.aggregate.consumer.service.ConsumerTaskFacade;
import org.springframework.web.reactive.function.client.WebClient;
import io.naradrama.consumer.aggregate.consumer.entity.Consumer;
import java.util.List;
import io.naradrama.shared.NameValueList;

@Component
public class ConsumerClient implements ConsumerTaskFacade {
    private static final String uri = "/consumer/consumers";
    private WebClient webClient;

    public ConsumerClient(WebClient webClient) {
        this.webClient = webClient;
    }

    public String test(){
        //
        StringBuilder uriStringBuilder = new StringBuilder();
        uriStringBuilder.append(uri + "/test");

        String message = webClient
                .get()
                .uri(uriStringBuilder.toString())
                .retrieve()
                .bodyToMono(String.class)
                .block();
        return message;
    }

    @Override
    public Consumer findConsumer(String consumerId) {
        return null;
    }

    @Override
    public List<Consumer> findAllConsumers() {
        return null;
    }

    @Override
    public void modifyConsumer(String consumerId, NameValueList nameValues) {
        return;
    }

    @Override
    public void removeConsumer(String consumerId) {
        return;
    }

    @Override
    public long countAllConsumer() {
        return 0l;
    }


}
