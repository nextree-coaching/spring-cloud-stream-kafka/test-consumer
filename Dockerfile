FROM java:8
ADD consumer-boot/build/libs/consumer-boot-1.0-SNAPSHOT.jar app.jar
RUN chmod +x app.jar
ARG SPRING_PROFILES_ACTIVE
RUN echo $SPRING_PROFILES_ACTIVE
ENV SPRING_PROFILES_ACTIVE=$SPRING_PROFILES_ACTIVE
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom",  "-jar","/app.jar"]
