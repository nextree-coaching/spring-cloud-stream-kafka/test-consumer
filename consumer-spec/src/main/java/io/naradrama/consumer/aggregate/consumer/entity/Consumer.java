package io.naradrama.consumer.aggregate.consumer.entity;

import io.naradrama.shared.JsonSerializable;
import lombok.Getter;
import lombok.Setter;
import lombok.NoArgsConstructor;
import io.naradrama.shared.JsonUtil;
import io.naradrama.shared.NameValueList;

@Getter
@Setter
@NoArgsConstructor
public class Consumer implements JsonSerializable {
    //
    private String id;
    private String name;
    private int sequence;
    private String email;

    public Consumer(String id) {
        //
        this.id = id;
    }

    public String toString() {
        return toJson();
    }

    public static Consumer fromJson(String json) {
        return JsonUtil.fromJson(json, Consumer.class);
    }

    public void setValues(NameValueList nameValues) {
    }

    public static Consumer sample() {
        return null;
    }
}
