package io.naradrama.consumer.client.aggregate.consumer.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.loadbalancer.reactive.LoadBalancerExchangeFilterFunction;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.web.reactive.function.client.WebClient;

@Slf4j
@Configuration
//@Profile({"local", "k8s"})
public class TenantWebClientConfig {
    //
    @Value("${nara.tenant.hostname:consumer}")
    String tenantHostname;
    @Value("${nara.tenant.protocol:http}")
    String tenantProtocol;
    @Value("${nara.tenant.port:8080}")
    String tenantPort;

    @Bean
    @Qualifier("tenantWebClient")
    public WebClient getTenantWebClient(LoadBalancerExchangeFilterFunction loadBalancerExchangeFilterFunction) {
        // create 방식
//        WebClient client_create = WebClient.create(RESOURCE_URL);
        // build 방식
        String baseUrl =  String.format("%s://%s", tenantProtocol, tenantHostname); //"http://tenant:8080";
        log.info("consumer host : {}", baseUrl);
        WebClient client = WebClient
                .builder()
                .filter(loadBalancerExchangeFilterFunction)
                .baseUrl(baseUrl)
                .build();

        return client;
    }

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertyPlaceholderConfigurer() {
        //
        return new PropertySourcesPlaceholderConfigurer();
    }
}
