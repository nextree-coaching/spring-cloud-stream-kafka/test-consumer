package io.naradrama.consumer.aggregate.consumer.service;

import io.naradrama.consumer.facade.aggregate.consumer.sdo.ConsumerCdo;
import io.naradrama.consumer.aggregate.consumer.entity.Consumer;
import java.util.List;
import io.naradrama.shared.NameValueList;

public interface ConsumerTaskService {
    String registerConsumer(ConsumerCdo consumerCdo);
    Consumer findConsumer(String consumerId);
    List<Consumer> findAllConsumers();
    void modifyConsumer(String consumerId, NameValueList nameValues);
    void removeConsumer(String consumerId);
    long countAllConsumer();
}
