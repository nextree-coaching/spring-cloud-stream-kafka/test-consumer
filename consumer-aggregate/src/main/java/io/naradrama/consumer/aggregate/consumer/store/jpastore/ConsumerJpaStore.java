package io.naradrama.consumer.aggregate.consumer.store.jpastore;

import org.springframework.stereotype.Repository;
import io.naradrama.consumer.aggregate.consumer.store.ConsumerStore;
import io.naradrama.consumer.aggregate.consumer.store.jpastore.repository.ConsumerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import io.naradrama.consumer.aggregate.consumer.entity.Consumer;
import io.naradrama.consumer.aggregate.consumer.store.jpastore.jpo.ConsumerJpo;
import java.util.Optional;
import java.util.List;
import io.naradrama.shared.Offset;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import java.util.stream.Collectors;

@Repository
public class ConsumerJpaStore implements ConsumerStore {
    @Autowired
    private ConsumerRepository consumerRepository;

    @Override
    public void create(Consumer consumer) {
        consumerRepository.save(new ConsumerJpo(consumer));
    }

    @Override
    public Consumer retrieve(String id) {
        Optional<ConsumerJpo> optionalConsumerJpo = consumerRepository.findById(id);
        return optionalConsumerJpo.map(ConsumerJpo::toDomain).orElse(null);
    }

    @Override
    public List<Consumer> retrieveAll(Offset offset) {
        Pageable pageable = PageRequest.of(offset.page(), offset.limit(), Sort.Direction.ASC, "name");
        Page<ConsumerJpo> consumerJpoPage = consumerRepository.findAll(pageable);
        return consumerJpoPage.stream().map(ConsumerJpo::toDomain).collect(Collectors.toList());
    }

    @Override
    public void update(Consumer consumer) {
        consumerRepository.save(new ConsumerJpo(consumer));
    }

    @Override
    public void delete(Consumer consumer) {
        consumerRepository.deleteById(consumer.getId());
    }

    @Override
    public boolean exists(String id) {
        return consumerRepository.existsById(id);
    }

    @Override
    public long countAll() {
        return consumerRepository.count();
    }
}
